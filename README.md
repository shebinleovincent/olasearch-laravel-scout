# Laravel Scout Engine for Ola Search

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE) [![Latest Stable Version](https://poser.pugx.org/shebinleovincent/olasearch-laravel-scout/v/stable.svg)](https://packagist.org/packages/shebinleovincent/olasearch-laravel-scout)


This package is the Laravel Scout Engine for [Ola Search](https://www.olasearch.com/).

## Contents

- [Installation](#installation)
- [Usage](#usage)
- [Notes](#notes)
- [Credits](#credits)
- [License](#license)

## Installation

You can install the package via composer:

``` bash
composer require shebinleovincent/olasearch-laravel-scout
```

Optionally, you can add the Scout service provider and the package service provider in the config/app.php file. Otherwise this can be done via automatic package discovery.

```php
// config/app.php
'providers' => [
    ...
    Laravel\Scout\ScoutServiceProvider::class,
    ...
    OlaSearchScout\OlaSearchServiceProvider::class,
],
```

### Setting up Ola Search configuration
You must have a API Key with Ola Search. If you don't have one, [sign-up](https://admin.olasearch.com/login) for a free account on Ola Search.

If you need help with this please refer to the [Ola Search documentation](https://olasearch.com/laravel-scout/)

After you've published the Laravel Scout package configuration:

```php
// config/scout.php
// Set your driver to olasearch
    'driver' => env('SCOUT_DRIVER', 'olasearch'),

...
    'olasearch' => [
        'id'  => env('OLASEARCH_PROJECT_ID', ''),
        'key' => env('OLASEARCH_API_KEY', ''),
    ],
...
```

## Usage

Now you can use Laravel Scout as described in the [official documentation](https://laravel.com/docs/5.6/scout)

## Notes

- Dev Branches are for development and are **UNSTABLE** (*use on your own risk*)!


## Credits

- [Shebin Leo Vincent](https://gitlab.com/shebinleovincent)
- [Ola Team](https://www.olasearch.com/)

## License

The MIT License (MIT).