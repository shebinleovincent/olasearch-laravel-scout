<?php
/**
 * Created by PhpStorm.
 * User: shebinleovincent
 * Date: 8/9/18
 * Time: 9:26 PM
 */

namespace OlaSearchScout;


use Laravel\Scout\Builder;
use Laravel\Scout\Engines\Engine;
use OlaSearch\Client as OlaSearch;
use Illuminate\Database\Eloquent\Collection;
use ReflectionClass;

class OlaSearchEngine extends Engine {
	/**
	 * The OlaSearch client.
	 *
	 * @var \OlaSearch\Client
	 */
	protected $olasearch;

	/**
	 * Create a new engine instance.
	 *
	 * @param  \OlaSearch\Client $olasearch
	 * @return void
	 */
	public function __construct( OlaSearch $olasearch ) {
		$this->olasearch = $olasearch;
	}

	/**
	 * Update the given model in the index.
	 *
	 * @param  \Illuminate\Database\Eloquent\Collection $models
	 * @throws \OlaSearch\OlaSearchException
	 * @throws \ReflectionException
	 * @return void
	 */
	public function update( $models ) {
		if ( $models->isEmpty() ) {
			return;
		}

		$index = $this->olasearch->initIndex();

		if ( $this->usesSoftDelete( $models->first() ) && config( 'scout.soft_delete', false ) ) {
			$models->each->pushSoftDeleteMetadata();
		}

		$documents = array();
		$mapping   = array();
		foreach ( $models as $model ) {
			$reflect = new ReflectionClass( $model );
			$array   = $model->toSearchableArray();

			if ( ! empty( $array ) ) {
				$meta        = array(
					'id'                  => $model->getScoutKey(),
					'ola_collection_slug' => $model->searchableAs(),
					'ola_collection_name' => $reflect->getShortName()
				);
				$documents[] = array_merge( $meta, $array );

				$mapping[$model->searchableAs()] = $model->scoutMetadata();
			}
		}

		$index->addDocuments( $documents, $mapping );
	}

	/**
	 * Remove the given model from the index.
	 *
	 * @param  \Illuminate\Database\Eloquent\Collection $models
	 * @return void
	 * @throws \OlaSearch\OlaSearchException
	 */
	public function delete( $models ) {
		$index = $this->olasearch->initIndex();

		$index->deleteDocuments(
			$models->map( function ( $model ) {
				return $model->getScoutKey();
			} )->values()->all()
		);
	}

	/**
	 * Perform the given search on the engine.
	 *
	 * @param  \Laravel\Scout\Builder $builder
	 * @return mixed
	 * @throws \OlaSearch\OlaSearchException
	 */
	public function search( Builder $builder ) {
		return $this->performSearch( $builder, array_filter( array(
			'numericFilters' => $this->filters( $builder ),
			'per_page'       => $builder->limit,
		) ) );
	}

	/**
	 * Perform the given search on the engine.
	 *
	 * @param  \Laravel\Scout\Builder $builder
	 * @param  int $perPage
	 * @param  int $page
	 * @return mixed
	 * @throws \OlaSearch\OlaSearchException
	 */
	public function paginate( Builder $builder, $perPage, $page ) {
		return $this->performSearch( $builder, array(
			'numericFilters' => $this->filters( $builder ),
			'per_page'       => $perPage,
			'page'           => $page,
		) );
	}

	/**
	 * Perform the given search on the engine.
	 *
	 * @param  \Laravel\Scout\Builder $builder
	 * @param  array $options
	 * @return mixed
	 * @throws \OlaSearch\OlaSearchException
	 */
	protected function performSearch( Builder $builder, array $options = array() ) {
		$olasearch = $this->olasearch->initIndex();

		if ( $builder->callback ) {
			return call_user_func(
				$builder->callback,
				$olasearch,
				$builder->query,
				$options
			);
		}

		return $olasearch->search( $builder->query, $options );
	}

	/**
	 * Get the filter array for the query.
	 *
	 * @param  \Laravel\Scout\Builder $builder
	 * @return array
	 */
	protected function filters( Builder $builder ) {
		return collect( $builder->wheres )->map( function ( $value, $key ) {
			return $key . '=' . $value;
		} )->values()->all();
	}

	/**
	 * Pluck and return the primary keys of the given results.
	 *
	 * @param  mixed $results
	 * @return \Illuminate\Support\Collection
	 */
	public function mapIds( $results ) {
		return collect( $results['results'] )->pluck( 'id' )->values();
	}

	/**
	 * Map the given results to instances of the given model.
	 *
	 * @param  mixed $results
	 * @param  \Illuminate\Database\Eloquent\Model $model
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function map( $results, $model ) {
		if ( count( $results['results'] ) === 0 ) {
			return Collection::make();
		}

		$models = $model->getScoutModelsByIds(
			collect( $results['results'] )->pluck( 'id' )->values()->all()
		)->keyBy( function ( $model ) {
			return $model->getScoutKey();
		} );

		return Collection::make( $results['results'] )->map( function ( $hit ) use ( $models ) {
			if ( isset( $models[$hit['id']] ) ) {
				return $models[$hit['id']];
			}
		} )->filter()->values();
	}

	/**
	 * Get the total count from a raw result returned by the engine.
	 *
	 * @param  mixed $results
	 * @return int
	 */
	public function getTotalCount( $results ) {
		return $results['totalResults'];
	}

	/**
	 * Determine if the given model uses soft deletes.
	 *
	 * @param  \Illuminate\Database\Eloquent\Model $model
	 * @return bool
	 */
	protected function usesSoftDelete( $model ) {
		return in_array( 'Illuminate\Database\Eloquent\SoftDeletes', class_uses_recursive( $model ) );
	}
}