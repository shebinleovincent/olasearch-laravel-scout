<?php
/**
 * Created by PhpStorm.
 * User: shebinleovincent
 * Date: 7/9/18
 * Time: 11:53 PM
 */

namespace OlaSearchScout;

use OlaSearch\Client as OlaSearch;
use Illuminate\Support\ServiceProvider;
use Laravel\Scout\EngineManager;

class OlaSearchServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		resolve( 'Laravel\Scout\EngineManager' )->extend( 'olasearch', function () {
			return new OlaSearchEngine( new OlaSearch(
				config( 'scout.olasearch.id' ),
				config( 'scout.olasearch.key' ),
				null,
				array(
					'env'    => config( 'scout.olasearch.env' ),
					'server' => config( 'scout.olasearch.server' ),
				)
			) );
		} );
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {

	}
}